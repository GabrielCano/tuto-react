import Page from "../../../core/components/page/page";

const GameExample = () => (
    <Page title="Game static example">
        <div className="game">
            <h1 className="game__title">Winner is X</h1>
            <div className="game__board board">
                <ul className="board__square-list">
                    <li className="board__square-item">
                        <button className="square">X</button>
                    </li>
                    <li className="board__square-item">
                        <button className="square">X</button>
                    </li>
                    <li className="board__square-item">
                        <button className="square">X</button>
                    </li>
                    <li className="board__square-item">
                        <button className="square">O</button>
                    </li>
                    <li className="board__square-item">
                        <button className="square">O</button>
                    </li>
                    <li className="board__square-item">
                        <button className="square"></button>
                    </li>
                    <li className="board__square-item">
                        <button className="square"></button>
                    </li>
                    <li className="board__square-item">
                        <button className="square"></button>
                    </li>
                    <li className="board__square-item">
                        <button className="square"></button>
                    </li>
                </ul>
            </div>
            <div className="game__controls">
                <button className="game__previous">previous</button>
                <button className="game__next">next</button>
            </div>
        </div>
    </Page>
)

export default GameExample;