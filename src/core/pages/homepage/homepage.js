import ClickCounter from "../../components/click-counter/click-counter";
import Clock from "../../components/clock/click-counter";
import Page from "../../components/page/page";
import Foodish from "../../components/foodish/foodish";
import "./homepage.scss";

const Homepage = () => (
    <Page title="homepage" >
        <div className="homepage">
            <div className="homepage__example">
                <h1 className="homepage__title">Click counter :</h1>
                <ClickCounter />
            </div>

            <div className="homepage__example">
                <h1 className="homepage__title">Clock :</h1>
                <Clock />
            </div>

            <div className="homepage__example">
                <h1 className="homepage__title">Foodish :</h1>
                <Foodish />
            </div>

        </div>
        
    </Page>
)

export default Homepage;