import { Link, useMatch, useResolvedPath } from "react-router-dom";
import classnames from "classnames";
import "./custom-link.scss";
import { useCallback } from "react";

function CustomLink({ children, className, currentClassName, to, ...props }) {
    let resolved = useResolvedPath(to);
    let match = useMatch({ path: resolved.pathname, end: true });

    const getClassName = useCallback(() => {
        let result = className || "";
        if(currentClassName){
            result = classnames(result, { [currentClassName]: match }); 
        }
        
        return result;
    }, [className, currentClassName, match])

    return  <Link className={getClassName()} to={to} {...props}>{children}</Link>;
}

export default CustomLink;