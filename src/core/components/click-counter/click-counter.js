import { useState } from "react";

const ClickCounter = () => {
    const [ count, setCount ] = useState(0);
    
    const handleClickCount = () => {
        setCount(count + 1);
    }

    return (
        <div className="click-counter" onClick={handleClickCount}>
            <button>Click Me ({count} click !)</button>
            
        </div>
    );
};

export default ClickCounter;