import { useEffect, useState } from "react"
import "./foodish.scss";

const Foodish = () => {
    const [ image, setImage ] = useState("");

    const getImage = () => {
        fetch("https://foodish-api.herokuapp.com/api")
        .then(response => response.json())
        .then(data => {
            setImage(data.image);
        })
    }
    useEffect(() => {
        getImage();
    }, []);

    return (
        <div className="foodish">
            {!image && <span>loading...</span>}
            {image && <img className="foodish__image" src={image} alt="food" />}
        </div>
    );
};

export default Foodish;