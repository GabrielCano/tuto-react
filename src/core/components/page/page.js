import React from "react";
import navigationList from "../../navigation.json";
import CustomLink from "../custom-link/custom-link";
import "./page.scss";


const Page = (props) => {
    const { title, children } = props;


    return (
        <div className="page">
            <header className="page__header">
                <h1 className="page__title">{title}</h1>
                <nav className="page__nav">
                    <ul className="page__nav-list">
                        {navigationList.map(({title, path}) => {
                            console.log({title, path});
                            return <li className="page__nav-item" key={path} ><CustomLink className="page__nav-item__link" currentClassName="page__nav-item__link--current" to={path}>{title}</CustomLink></li>
                        })}
                    </ul>
                </nav>
            </header>
            <main className="page__main">{children}</main>
        </div>
    )
}

export default Page;