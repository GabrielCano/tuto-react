import "./style/index.scss";

export { default as Page } from "./components/page/page";
export { default as Homepage } from "./pages/homepage/homepage";

export { default as CustomLink } from "./components/custom-link/custom-link";
export { default as ClickCounter } from "./components/custom-link/custom-link";
export { default as Clock } from "./components/custom-link/custom-link";
export { default as Foodish } from "./components/foodish/foodish";