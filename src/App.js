import { BrowserRouter, Routes, Route } from "react-router-dom";
import { GameExample } from "./example";
import { YourPage } from "./practice";
import { Homepage } from "./core";

const  App = () => {
  console.log("App");
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Homepage />} />
        <Route path="/example" element={<GameExample />} />
        <Route path="/practice" element={<YourPage />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
