export { default as YourPage } from "./page/your-page";

export { default as Board } from "./components/board/board";
export { default as Game } from "./components/game/game";
export { default as Square } from "./components/square/square";