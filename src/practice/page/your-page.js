import { Page } from "../../core";
import Game from "../components/game/game";

const YourPage = () => (
    <Page title="Practice Page">
        <Game />
    </Page>
);

export default YourPage;